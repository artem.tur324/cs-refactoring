import math
import sys

'''
Необхідно знайти Z1, Z2, Z3, якщо:
Z1 = (cos(y) - cos(x))^2 - 5
Z2 = 4 * (sin((x - y) / 2*x))^2
Z3 = (5 + Z2) / Z1
'''

def main():

    x = conversion_to_float(input("Введіть значення x: "))
    y = conversion_to_float(input("Введіть значення y: "))
    z1 = (math.cos(y) - math.cos(x)) ** 2 - 5
    d = 2 * x
    check_equality_0(d)
    z2 = 4 * (math.sin((x - y) / d)) ** 2
    check_equality_0(z1)
    z3 = (5 + z2) / z1
    print(f"Z1 = {z1}")
    print(f"Z2 = {z2}")
    print(f"Z3 = {z3}")
    sys.exit(0)

def check_equality_0(value):
    if value == 0:
        print("Некоректні дані. Знаменник не може дорівнювати нулю.")
        sys.exit(1)
def conversion_to_float(value):
    try:
        value = float(value)
    except ValueError:
        print("Некоректні дані. Введіть числові значення для x та y.")
        sys.exit(1)
    return value

if __name__ == "__main__":
    main()
