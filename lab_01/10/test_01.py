import unittest
from unittest.mock import patch
from io import StringIO
from task_01 import main

class TestMain(unittest.TestCase):
    @patch('sys.stdin', StringIO('dummy\n'))
    @patch('sys.stdout', new_callable = StringIO)
    def test_invalid_input(self, stdout):
        with self.assertRaises(SystemExit) as cm:
            main()
        self.assertEqual(cm.exception.code, 1)
        self.assertEqual(stdout.getvalue(), "Введіть довжину першого катета: Некоректні дані. Введіть числові значення для довжин катетів.\n")

    @patch('sys.stdin', StringIO('9\n4\n'))
    @patch('sys.stdout', new_callable = StringIO)
    def test_success_1(self, stdout):
        with self.assertRaises(SystemExit) as cm:
            main()
        self.assertEqual(cm.exception.code, 0)
        self.assertEqual(stdout.getvalue(), "Введіть довжину першого катета: Введіть довжину другого катета: Довжина гіпотенузи: 9.848857801796104\n")

    @patch('sys.stdin', StringIO('-7\n7\n'))
    @patch('sys.stdout', new_callable = StringIO)
    def test_success(self, stdout):
        with self.assertRaises(SystemExit) as cm:
            main()
        self.assertEqual(cm.exception.code, 1)
        self.assertEqual(stdout.getvalue(), "Введіть довжину першого катета: Некоректні дані. Введіть додатнє число.\n")
