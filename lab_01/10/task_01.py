import math
import sys

'''
Обчислити довжину гіпотенузи прямокутного трикутника, якщо задано
довжини катетів.
'''

def get_positive_float(prompt):

    try:
        value = float(input(prompt))
        if value > 0:
            return value
        else:
            print("Некоректні дані. Введіть додатнє число.")
            sys.exit(1)
    except ValueError:
        print("Некоректні дані. Введіть числові значення для довжин катетів.")
        sys.exit(1)

def calculate_hypotenuse(a, b):

    if a is None or b is None:
        return None
    return math.sqrt(a ** 2 + b ** 2)

def main():

        a = get_positive_float("Введіть довжину першого катета: ")
        b = get_positive_float("Введіть довжину другого катета: ")

        h = calculate_hypotenuse(a, b)

        print(f"Довжина гіпотенузи: {h}")
        sys.exit(0)

if __name__  == "__main__":
    main()