import unittest
from unittest.mock import patch
from io import StringIO
import sys
import re
from task_02 import main


class TestTask_01(unittest.TestCase):

    def string_console_result(self, console_output):
        string_to_remove_x = "Введіть значення x: "
        string_to_remove_y = "Введіть значення y: "
        result_string = console_output.replace(string_to_remove_x, "").replace(string_to_remove_y, "").replace(
            "  ", " ")
        return result_string

    def find_text_between(self, number_z, input_text):
        pattern = f'{number_z} (.*?)\n'
        match = re.search(pattern, input_text)
        if match:
            return match.group(1).strip()
        return None

    @patch('sys.stdin', StringIO('4\n7\n'))
    @patch('sys.stdout', new_callable = StringIO)
    def test_z1_x_4_y_7(self, stdout):
        with self.assertRaises(SystemExit) as cm:
            main()
        self.assertEqual(cm.exception.code, 0)
        actual_Output = self.find_text_between("Z1 =",stdout.getvalue())
        expected_Output = "-3.018814609187995"
        self.assertEqual(actual_Output, expected_Output)

    @patch('sys.stdin', StringIO('4\n7\n'))
    @patch('sys.stdout', new_callable=StringIO)
    def test_z2_x_4_y_7(self, stdout):
        with self.assertRaises(SystemExit) as cm:
            main()
        self.assertEqual(cm.exception.code, 0)
        actual_Output = self.find_text_between("Z2 =", stdout.getvalue())
        expected_Output = "0.5366222622523582"
        self.assertEqual(actual_Output, expected_Output)

    @patch('sys.stdin', StringIO('4\n7\n'))
    @patch('sys.stdout', new_callable=StringIO)
    def test_z3_x_4_y_7(self, stdout):
        with self.assertRaises(SystemExit) as cm:
            main()
        self.assertEqual(cm.exception.code, 0)
        actual_Output = self.find_text_between("Z3 =", stdout.getvalue())
        expected_Output = "-1.83403851478697"
        self.assertEqual(actual_Output, expected_Output)

    @patch('sys.stdin', StringIO('0\n4\n'))
    @patch('sys.stdout', new_callable=StringIO)
    def test_division_4by0_error_returned(self, stdout):
        with self.assertRaises(SystemExit) as cm:
            main()
        self.assertEqual(cm.exception.code, 1)
        actual_Output = self.string_console_result(stdout.getvalue())
        expected_Output = "Некоректні дані. Знаменник не може дорівнювати нулю.\n"
        self.assertEqual(actual_Output, expected_Output)

    @patch('sys.stdin', StringIO('A\n3\n'))
    @patch('sys.stdout', new_callable=StringIO)
    def test_division_Aby3_FormatException_returned(self, stdout):
        with self.assertRaises(SystemExit) as cm:
            main()
        self.assertEqual(cm.exception.code, 1)
        actual_Output = self.string_console_result(stdout.getvalue())
        expected_Output = "Некоректні дані. Введіть числові значення для x та y.\n"
        self.assertEqual(actual_Output, expected_Output)

if __name__ == '__main__':
    unittest.main()
