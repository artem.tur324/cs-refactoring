import sys

'''
Обчислити висоту циліндра, якщо задано його об’єм та площа основи.
'''


def main():
    try:
        volume = float(input("Введіть об'єм циліндра: "))
        base_area = float(input("Введіть площу основи циліндра: "))
        if base_area > 0:
            h = volume / base_area
        else:
            h = "Некоректні дані. Площа основи циліндра повинна бути додатньою."
        if isinstance(h, float):
            print(f"Висота циліндра: {h}")
            sys.exit(0)
        else:
            print(h)
            sys.exit(1)
    except ValueError:
        print("Некоректні дані. Введіть числові значення для об'єму та площі основи циліндра.")
        sys.exit(1)


if __name__ == "__main__":
    main()
