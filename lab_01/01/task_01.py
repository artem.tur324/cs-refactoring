import sys
import math

"""
Обчислити площу квадрату, якщо задано його діагональ.
"""


def main():
    try:
        v1 = float(input('Введіть діагональ квадрату:'))
        if v1 <= 0:
            print(f"Довжина діагоналі ({v1}) не може бути менше або дорівнювати 0.")
            sys.exit(1)
        else:
            v2 = math.sqrt(v1 ** 2 / 2) * math.sqrt(v1 ** 2 / 2)
            print(f"Площа квадрата з діагоналлю {v1} дорівнює {v2}.")
            sys.exit(0)
    except ValueError:
        print('Введене значення не є числом.')
        sys.exit(1)


ghjdg

if __name__ == '__main__':
    main()
